# PHP Book Search

## Installation

Place all files so that ```html``` directory from the project is the web directory. Other files and directories should not be visible from web.

Find ```books.sql``` file and create database structure. Default owner user is ```librarian``` as set in ```config.php``` file. You should either create it or modify ```config.php``` for another username. The same applies for other settings in the file.

## Working with database

A few test subdirectories and XML files are placed in the ```books``` directory. Directory containing XML files can be easily changed in ```config.php``` for different tests.

```cron/scan.php``` is designed to purge and scan files to database. It takes two options - ```-p``` and ```-s```.

```-p``` - Purge database, prepare it for a new scan.  
```-s [count]``` - Collect files to database. Parse up to ```count``` files to database, or all of the collected files if no ```count``` is set. Limit is designed to help when script is executed as a cron job and needs to work with a large amount of data, so it can be split in chunks.

## Webpage

There's only one PHP script - ```index.php``` that should be loaded in the browser. It searches in database for author's name and displays all the authors and their books that match.

It also should work with Cyrillic, Korean, and Japanese symbols (and other extended character sets too) in XML files.
