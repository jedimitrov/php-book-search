<?php

class Parser
{
    private $model;

    public function __construct(&$model)
    {
        $this->model = $model;
    }

    /* Read number of paths from database (or all, if no limit is set),
       parse and save books and authors to database */
    public function parse($count=null)
    {
        $files = $this->model->getUnparsedXMLFiles($count);

        foreach ($files as $file_id => $path) {
            if (($xmlcode = file_get_contents($path)) === false)
                throw new Exception('Unable to read from file ' . $path, 21);

            $xml = new SimpleXMLElement($xmlcode);

            if (isset($xml->book))
                foreach ($xml->book as $book)
                    $this->model->addBook($book->name, $book->author);

            $this->model->markXMLParsed($file_id);
        }

    }
};

?>
