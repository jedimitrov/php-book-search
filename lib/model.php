<?php

require_once dirname(__FILE__) . '/../config.php';

/**
 * Database model class.
 */
class Model
{
    private $conn;

    /* Connect to database */
    public function __construct()
    {
        if (!($this->conn = $this->connect()))
            throw new Exception('Unable to connect to database', 1);
    }

    /* Connect helper function */
    private function connect()
    {
        global $dbhost, $dbuser, $dbpass, $dbname;

        $connstr = "host={$dbhost} user={$dbuser} password={$dbpass} dbname={$dbname}";
        return @pg_connect($connstr);
    }

    /* Purge all tables, reset identity counters */
    public function purge()
    {
        $res = pg_query(
            $this->conn,
            'TRUNCATE xml_files, books, authors RESTART IDENTITY'
        );
        if (!$res)
            throw new Exception('Unable to truncate tables', 99);
    }

    /* Add author name and book title to database */
    public function addBook($book_title, $author_name)
    {
        /* Add author first, duplicate is ok, get author Id */
        $res = pg_query_params(
            $this->conn,
            'INSERT INTO authors (name) ' .
            'VALUES ($1) ' .
            'ON CONFLICT (name) DO ' .
            '    UPDATE SET name=EXCLUDED.name ' .
            'RETURNING author_id',
            [ $author_name ]
        );
        if (!$res)
            throw new Exception('Unable to add author to database', 2);

        $row = pg_fetch_assoc($res);
        $author_id = $row['author_id'];

        /* Add book, duplicate is ok, get and return book Id */
        $res = pg_query_params(
            $this->conn,
            'INSERT INTO books (author_id, title) ' .
            'VALUES ($1, $2) ' .
            'ON CONFLICT ON CONSTRAINT books_author_id_title_key DO ' .
            '    UPDATE SET title=EXCLUDED.title ' .
            'RETURNING book_id',
            [ $author_id, $book_title ]
        );
        if (!$res)
            throw new Exception('Unable to add book to database', 3);

        $row = pg_fetch_assoc($res);
        return $row['book_id'];
    }

    /* Add XML file path to database */
    public function addXMLFile($path)
    {
        $res = pg_query_params(
            $this->conn,
            'INSERT INTO xml_files (path) ' .
            'VALUES ($1) ' .
            'ON CONFLICT DO NOTHING',
            [ $path ]
        );
        if (!$res)
            throw new Exception('Unable to add XML path to database', 4);
    }

    /* Get a limited number of unparsed XML files from database, or all of them if no limit is set */
    public function getUnparsedXMLFiles($count=null)
    {
        if (!$count)
            $res = pg_query(
                $this->conn,
                'SELECT file_id, path FROM xml_files WHERE parsed=FALSE',
            );
        else
            $res = pg_query_params(
                $this->conn,
                'SELECT file_id, path FROM xml_files WHERE parsed=FALSE LIMIT $1',
                [ (int) $count ]
            );
        if (!$res)
            throw new Exception('Unable to read XML files from database', 5);

        $ret = array();
        while ($row = pg_fetch_assoc($res))
            $ret[$row['file_id']] = $row['path'];

        return $ret;
    }

    /* Mark XML file as parsed (by Id) */
    public function markXMLParsed($file_id)
    {
        $res = pg_query_params(
            $this->conn,
            'UPDATE xml_files SET parsed=TRUE WHERE file_id=$1',
            [ (int) $file_id ]
        );
        if (!$res)
            throw new Exception('Unable to mark XML as parsed in database', 6);
    }

    /* Return number of files in database */
    public function numberOfFiles()
    {
        $res = pg_query(
            $this->conn,
            'SELECT COUNT(file_id) AS fcnt FROM xml_files'
        );
        if (!$res)
            throw new Exception('Unable to read number of XML files in database', 7);

        $row = pg_fetch_assoc($res);
        return $row['fcnt'];
    }

    public function search($query)
    {
        $res = pg_query_params(
            $this->conn,
            'SELECT authors.name AS author_name, books.title AS book_title ' .
            'FROM authors, books ' .
            'WHERE ' .
                'to_tsvector(authors.name) @@ to_tsquery($1) AND ' .
                'books.author_id=authors.author_id',
            [ $query ]
        );
        if (!$res)
            throw new Exception('Unable to search in database', 8);

        $ret = array();
        while ($row = pg_fetch_assoc($res))
            $ret[] = $row;

        return $ret;
    }
}

?>
