<?php

class Scanner
{
    private $model;
    private $root;

    public function __construct(&$model, $dir)
    {
        $this->model = $model;
        $this->root = $dir;
    }

    /* Recursively scan all directories and subdirectories for XML files, save them to database */
    public function scan($dir=null)
    {
        if (!$dir)
            $dir = $this->root;

        if ($dd = opendir($dir)) {
            while (($file = readdir($dd)) !== false) {
                if ($file == '.' || $file == '..')
                    continue;
                $path = "{$dir}/{$file}";
                if (is_dir($path))
                    $this->scan($path);
                elseif (is_file($path)) {
                    $parts = pathinfo($path);
                    if (strtolower($parts['extension']) == 'xml')
                        $this->model->addXMLFile($path);
                }
            }
        }
    }
};

?>
