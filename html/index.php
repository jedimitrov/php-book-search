<!DOCTYPE html>
<?php

require_once dirname(__FILE__) . '/../config.php';

require_once dirname(__FILE__) . '/../lib/model.php';
require_once dirname(__FILE__) . '/../lib/scanner.php';
require_once dirname(__FILE__) . '/../lib/parser.php';

$query = isset($_GET['query']) ? $_GET['query'] : "";
$hquery = htmlentities($query);

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Book Search</title>
        <link rel="stylesheet" href="default.css">
        <script type="text/javascript" src="search.js"></script>
    </head>

    <body>
        <div id="page">
            <form method="get">
                <h2>Book Search</h2>
                <input type="text" name="query" value="<?php echo $hquery ?>" /><input type="submit" value="Search" />
            </form>

            <div id="list">
                <?php
                    if (isset($_GET['query'])) {
                        try {
                            $model = new Model();
                            $results = $model->search($_GET['query']);
                            $delay = 0;

                            foreach ($results as $res) {
                                $author = htmlentities($res['author_name']);
                                $book = htmlentities($res['book_title']);
                                ?>
                                <div class="result" style="animation-delay: <?=$delay/10?>s">
                                    <div class="field"><?php echo $author ?></div><div class="field"><?php echo $book ?></div>
                                </div>
                                <?php
                                $delay++;
                            }
                        }
                        catch (Exception $e) {
                            die($e->getMessage());
                        }
                    }
                ?>
            </list>
        </div>
    </body>
</html>
