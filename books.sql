--
-- PostgreSQL database dump
--

-- Dumped from database version 13.8 (Debian 13.8-0+deb11u1)
-- Dumped by pg_dump version 13.8 (Debian 13.8-0+deb11u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: authors; Type: TABLE; Schema: public; Owner: librarian
--

CREATE TABLE public.authors (
    author_id integer NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE public.authors OWNER TO librarian;

--
-- Name: authors_author_id_seq; Type: SEQUENCE; Schema: public; Owner: librarian
--

CREATE SEQUENCE public.authors_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.authors_author_id_seq OWNER TO librarian;

--
-- Name: authors_author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: librarian
--

ALTER SEQUENCE public.authors_author_id_seq OWNED BY public.authors.author_id;


--
-- Name: books; Type: TABLE; Schema: public; Owner: librarian
--

CREATE TABLE public.books (
    book_id integer NOT NULL,
    author_id integer NOT NULL,
    title character varying(128) NOT NULL
);


ALTER TABLE public.books OWNER TO librarian;

--
-- Name: books_book_id_seq; Type: SEQUENCE; Schema: public; Owner: librarian
--

CREATE SEQUENCE public.books_book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.books_book_id_seq OWNER TO librarian;

--
-- Name: books_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: librarian
--

ALTER SEQUENCE public.books_book_id_seq OWNED BY public.books.book_id;


--
-- Name: xml_files; Type: TABLE; Schema: public; Owner: librarian
--

CREATE TABLE public.xml_files (
    file_id integer NOT NULL,
    path character varying(4096) NOT NULL,
    parsed boolean DEFAULT false NOT NULL
);


ALTER TABLE public.xml_files OWNER TO librarian;

--
-- Name: xml_files_file_id_seq; Type: SEQUENCE; Schema: public; Owner: librarian
--

CREATE SEQUENCE public.xml_files_file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xml_files_file_id_seq OWNER TO librarian;

--
-- Name: xml_files_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: librarian
--

ALTER SEQUENCE public.xml_files_file_id_seq OWNED BY public.xml_files.file_id;


--
-- Name: authors author_id; Type: DEFAULT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.authors ALTER COLUMN author_id SET DEFAULT nextval('public.authors_author_id_seq'::regclass);


--
-- Name: books book_id; Type: DEFAULT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.books ALTER COLUMN book_id SET DEFAULT nextval('public.books_book_id_seq'::regclass);


--
-- Name: xml_files file_id; Type: DEFAULT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.xml_files ALTER COLUMN file_id SET DEFAULT nextval('public.xml_files_file_id_seq'::regclass);


--
-- Name: authors authors_name_key; Type: CONSTRAINT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.authors
    ADD CONSTRAINT authors_name_key UNIQUE (name);


--
-- Name: authors authors_pkey; Type: CONSTRAINT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.authors
    ADD CONSTRAINT authors_pkey PRIMARY KEY (author_id);


--
-- Name: books books_author_id_title_key; Type: CONSTRAINT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_author_id_title_key UNIQUE (author_id, title);


--
-- Name: books books_pkey; Type: CONSTRAINT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_pkey PRIMARY KEY (book_id);


--
-- Name: xml_files xml_files_path_key; Type: CONSTRAINT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.xml_files
    ADD CONSTRAINT xml_files_path_key UNIQUE (path);


--
-- Name: xml_files xml_files_pkey; Type: CONSTRAINT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.xml_files
    ADD CONSTRAINT xml_files_pkey PRIMARY KEY (file_id);


--
-- Name: books books_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: librarian
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.authors(author_id);


--
-- PostgreSQL database dump complete
--

