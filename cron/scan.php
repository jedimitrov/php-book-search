<?php

require_once dirname(__FILE__) . '/../config.php';

require_once dirname(__FILE__) . '/../lib/model.php';
require_once dirname(__FILE__) . '/../lib/scanner.php';
require_once dirname(__FILE__) . '/../lib/parser.php';

if (count($argv) < 2)
    die("Usage: {$argv[0]} -p | -s [count]\n");

try {
    switch ($argv[1]) {

        /* Purge database, prepare for new scan */
        case '-p':
            $model = new Model();
            $model->purge();
            break;

        /* Scan, parse and save books in database */
        case '-s':
            $model = new Model();
            $parser = new Parser($model);

            /* Rescan directories if paths are not saved in database */
            if ($model->numberOfFiles() == 0) {
                $scanner = new Scanner($model, $book_dir);
                $scanner->scan();
            }

            /* Parse given number of files to database, or all files if no limit is set */
            if (count($argv) > 2)
                $parser->parse((int) $argv[2]);
            else
                $parser->parse();

            break;

        default:
            die("Usage: {$argv[0]} -p | -s <count>\n");
            break;
    }
}

catch (Exception $e) {
    echo "{$e->getMessage()}\n";
    exit($e->getCode());
}

?>
